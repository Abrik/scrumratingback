﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScrumRatingBack.Data;
using ScrumRatingBack.Model;

namespace ScrumRatingBack.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly StudentContext _context;
        private readonly IMapper _mapper;

        public StudentController(IConfiguration config, StudentContext context, IMapper mapper)
        {
            _config = config;
            _context = context;
            _mapper = mapper;
        }

        private readonly ILogger<StudentController> _logger;

        
        [HttpGet("{id}", Name = "GetStudent")]
        public ActionResult<Student> GetById(int id)
        {
            var item = _context.Students.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [Route("list/{id}")]
        [HttpGet]
        public PagedResult<Student> List([FromQuery]int page)
        {
            int pageSize = 10;
            return _context.Students.GetPaged(page, pageSize);
        }
        /*public PagedResult<Student> List([FromQuery]int page)
        {
            int pageSize = 10;
            // Do not send password over webAPI GET
            foreach (User u in _context.Users)
            {
                u.Password = string.Empty;
            }
            return _context.Users
                .OrderBy(p => p.Id)
                .GetPaged(page, pageSize);
        }*/

    }
}