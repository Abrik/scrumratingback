﻿using ScrumRatingBack.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumRatingBack.Data
{
    public class DataInitializer
    {
        public static void Initialize(StudentContext studentContext)
        {
            if (studentContext.Students.Count() == 0)
            {
                studentContext.Students.Add(new Student(1, "Иванов", "Иван", "Иванович"));
                studentContext.Students.Add(new Student(2, "Петров", "Петр", "Петрович"));
                studentContext.SaveChanges();
            }
        }
    }
}
