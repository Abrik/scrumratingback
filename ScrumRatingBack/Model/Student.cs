﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScrumRatingBack.Model
{
    public class Student
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Secondname { get; set; }

        public Student(int id, string surname, string firstname, string secondname)
        {
            Id = id;
            Surname = surname;
            Firstname = firstname;
            Secondname = secondname;
        }
    }
}
